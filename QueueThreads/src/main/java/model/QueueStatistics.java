package model;

public class QueueStatistics {
	private int clientNr;
	private int waitingTime;
	private int serviceTime;
	private int emptyTime;
	
	/**
	 * Constructor for the QueueStatistics object
	 */
	public QueueStatistics(){
		clientNr=0;
		waitingTime=0;
		serviceTime=0;
		emptyTime=0;
	}
	
	/**
	 * Increases the emptyTime value with 200
	 */
	public synchronized void incEmptyTime(){
		emptyTime+=200;
	}
	
	/**
	 * Gets the information from a Client object and adds it to the current information
	 * 
	 * @param c the Client whose information is to be extracted
	 */
	public void addClient(Client c){
		clientNr++;
		waitingTime+=c.getWaitingTime();
		serviceTime+=c.getServiceTime();
	}
	
	@Override
	public String toString(){
		return "Average waiting time: "+((double)(clientNr!=0?waitingTime/clientNr:0)/200)+
				" minutes; Average service time: "+((double)(clientNr!=0?serviceTime/clientNr:0)/200)+
				" minutes; Empty time: "+emptyTime/200+" minutes";
	}
}

