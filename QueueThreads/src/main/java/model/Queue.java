package model;

import java.util.List;
import java.util.Vector;

import view.Printable;

public class Queue implements Runnable{
	private List<Client> queue;
	private boolean run;
	private String name;
	private QueueStatistics qS;
	private Printable logger;
	
	/**
	 * Constructor for the Queue objects
	 * 
	 * @param name the name of the Queue
	 * @param qS the QueueStatistics object associated with the Queue
	 * @param l the Printable object that displays the log of events
	 */
	public Queue(String name, QueueStatistics qS, Printable l){
		queue=new Vector<Client>();
		run=true;
		this.name=name;
		this.qS=qS;
		logger=l;
	}
	
	/**
	 * Adds a Client object to the Vector of Client objects
	 * 
	 * @param c the Client object to be added
	 */
	public void addClient(Client c){
		c.changeWaitingTime(getTotalWaitingTime());
		queue.add(c);
	}
	
	/**
	 * Removes the first Client object from the queue, adding it to the QueueStatistics 
	 * object in order to retrieve the data from it. Also prints a message.
	 */
	private void removeClient(){
		Client c=queue.remove(0);
		qS.addClient(c);
		logger.print("Client removed from "+name+" with waiting time "+((double)c.getWaitingTime()/200)+" minute(s)");
	}
	
	/**
	 * Returns the total waiting time for the queue
	 * 
	 * @return the sum of all the waiting times for all the Client objects in the Vector
	 */
	public int getTotalWaitingTime(){
		int sum=0;
		for(Client c: queue){
			sum+=c.getServiceTime();
		}
		return sum;
	}
	
	/**
	 * Returns the number of clients in the queue
	 * 
	 * @return the number of Client objects in the Vector
	 */
	public int getNrOfClients(){
		return queue.size();
	}
	
	/**
	 * Sets the run flag to false
	 */
	public void stopQueue(){
		run=false;
	}
	
	/**
	 * This method is accessed when the thread is started. It runs in a loop,
	 * removing clients from the queue until the simulation time ends and there
	 * are no more clients in the queue
	 */
	public void run(){
		while (run || !queue.isEmpty()) {
			if (queue.size()!=0) {
				try {
					Thread.sleep(queue.get(0).getServiceTime());
				} catch (InterruptedException e) {
					//do nothing
				}
				removeClient();
			}
			else{
				try {
					qS.incEmptyTime();
					Thread.sleep(200);
				} catch (InterruptedException e) {
					//do nothing
				}
			}
		}
		logger.print(name+" closed ");
	}
	
	@Override
	public String toString(){
		return name+": "+qS;
	}
}
