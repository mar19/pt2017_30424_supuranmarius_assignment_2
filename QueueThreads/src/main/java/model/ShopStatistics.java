package model;

public class ShopStatistics {
	private int peakClients;
	private int peakHour;
	private int startHour;
	
	/**
	 * Constructor for the ShopStatistics object
	 * 
	 * @param sH the starting hour
	 */
	public ShopStatistics(int sH){
		peakClients=0;
		peakHour=0;
		startHour=sH;
	}
	
	/**
	 * Checks if the clients parameter is greater than peakClients.
	 * If yes, it updates peakClients and peakHour
	 * 
	 * @param clients the new number of client
	 * @param hour the hour
	 */
	public void changePeakClients(int clients, int hour){
		if(clients>=peakClients){
			peakClients=clients;
			peakHour=hour;
		}
	}
	
	@Override
	public String toString(){
		return "Peak nr of clients: "+peakClients+
				" at "+(startHour+peakHour)/12000+" o'clock";
	}
}
