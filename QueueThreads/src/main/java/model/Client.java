package model;

public class Client {
	private int serviceTime;
	private int waitingTime;
	
	/**
	 * Constructor for the Client object
	 * 
	 * @param sT the serviceTime
	 */
	public Client(int sT){
		serviceTime=sT;
		waitingTime=sT;
	}

	/**
	 * getter: returns the serviceTime
	 * 
	 * @return the serviceTime
	 */
	public int getServiceTime() {
		return serviceTime;
	}
	
	/**
	 * Increases the waiting field
	 * 
	 * @param wT the amount with which the field is increased
	 */
	public void changeWaitingTime(int wT){
		waitingTime+=wT;
	}

	/**
	 * getter: returns the waitingTime
	 * 
	 * @return the waitingTime
	 */
	public final int getWaitingTime() {
		return waitingTime;
	}
}
