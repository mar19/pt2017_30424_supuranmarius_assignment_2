package model;

import java.util.concurrent.*;

import view.Printable;

public class Model {
	private int startingHour;
	private int runningTime;
	private int minArrTime;
	private int maxArrTime;
	private int minServTime;
	private int maxServTime;
	private int nrOfQueues;
	
	/**
	 * Method used to set the data in the object
	 * 
	 * @param sH the starting hour
	 * @param rT the running time
	 * @param minAT the minimum arrival time
	 * @param maxAT the maximum arrival time
	 * @param minST the minimum service time
	 * @param maxST the maximum service time
	 * @param nQ the number of queues
	 */
	public void setData(int sH, int rT, int minAT, int maxAT, int minST, int maxST, int nQ){
		startingHour=sH*12000;
		runningTime=rT*12000;
		minArrTime=minAT*200;
		maxArrTime=maxAT*200;
		minServTime=minST*200;
		maxServTime=maxST*200;
		nrOfQueues=nQ;
	}
	
	/**
	 * Method used to start the threads, wait for their execution to end and output the results
	 * 
	 * @param logger the Printable object to display the log of events
	 * @param results the Printable object to display the results
	 */
	public void startQueues(Printable logger, Printable results){
		ShopStatistics sS=new ShopStatistics(startingHour);
		QueueStatistics qS[]=new QueueStatistics[nrOfQueues];
		Queue[] q=new Queue[nrOfQueues];
		for(int i=0;i<nrOfQueues;i++){
			qS[i]=new QueueStatistics();
		}
		for(int i=0;i<nrOfQueues;i++){
			q[i]=new Queue("Queue "+(i+1),qS[i],logger);
		}
		ClientProducer cp=new ClientProducer(minArrTime,maxArrTime,minServTime,maxServTime,q,sS,runningTime,logger);
		ExecutorService exec = Executors.newFixedThreadPool(nrOfQueues);
		for(Queue c:q){
			exec.execute(c);
		}
		new Thread(cp).start();
		exec.shutdown();
		try {
			  exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			  //nothing
		}
		for(Queue c:q){
			results.print(c.toString());
		}
		results.print(cp.toString());
	}
}
