package model;

import view.Printable;

public class ClientProducer implements Runnable{
	
	private int minArrTime;
	private int maxArrTime;
	private int minServTime;
	private int maxServTime;
	private Queue[] queues;
	private ShopStatistics sS;
	private int runningTime;
	private int startingRunningTime;
	private Printable logger;
	
	/**
	 * Constructor for the ClientProducer object
	 * 
	 * @param minAT the minimum arrival time
	 * @param maxAT the maximum arrival time
	 * @param minST the minimum service time
	 * @param maxST the maximum service time
	 * @param queues the array of Queue objects
	 * @param sS the ShopStatistics object
	 * @param rT the simulation running time
	 * @param l the Printable object
	 */
	public ClientProducer(int minAT, int maxAT, int minST, int maxST, Queue[] queues, ShopStatistics sS, int rT, Printable l){
		minArrTime=minAT;
		maxArrTime=maxAT;
		minServTime=minST;
		maxServTime=maxST;
		this.queues=queues;
		this.sS=sS;
		runningTime=rT;
		startingRunningTime=rT;
		logger=l;
	}

	/**
	 * This method is accessed when the thread is started. It runs in a loop,
	 * adding clients to the appropriate queue until the simulation time ends.
	 */
	public void run() {
		while(true){
			try {
				int rand1=(int)(Math.random()*(maxArrTime-minArrTime)+minArrTime);
				Thread.sleep(rand1);
				runningTime-=rand1;
				if(runningTime<=0){
					for(Queue q:queues){
						q.stopQueue();
					}
					logger.print("No more clients to be added");
					return;
				}
				int rand=(int)(Math.random()*(maxServTime-minServTime)+minServTime);
				int imin=getIndexOfMin();
				queues[imin].addClient(new Client(rand));
				logger.print("Client added to Queue "+(imin+1)+" with service time "+((double)rand/200)+" minute(s)");
				sS.changePeakClients(getTotalClients(),startingRunningTime-runningTime);
			} catch (InterruptedException e) {
				//do nothing
			}
		}
	}
	
	/**
	 * This method is used to get the index of the Queue object with the minimal
	 * waiting time
	 * 
	 * @return the index of the Queue object in the array
	 */
	private int getIndexOfMin(){
		int[] qLength=new int[queues.length];
		int min, imin;
		min=0;
		for(int i=0;i<qLength.length;i++){
			qLength[i]=queues[i].getTotalWaitingTime();
		}
		min=qLength[0];imin=0;
		for(int i=1;i<qLength.length;i++){
			if(qLength[i]<min){
				imin=i;
				min=qLength[i];
			}
		}
		return imin;
	}
	
	/**
	 * This method is used to get the total number of Client objects in the simulation
	 * 
	 * @return the total number of clients
	 */
	private int getTotalClients(){
		int total=0;
		for(Queue q:queues){
			total+=q.getNrOfClients();
		}
		return total;
	}
	
	@Override
	public String toString(){
		return "Shop statistics: "+sS;
	}
}
