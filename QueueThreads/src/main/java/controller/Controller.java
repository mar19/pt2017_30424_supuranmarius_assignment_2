package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.*;
import view.*;

public class Controller {
	private Model model;
	private MainPage view;
	
	/**
	 * Constructor for the Controller object
	 * 
	 * @param model the Model
 	 * @param view the View
	 */
	public Controller(Model model, MainPage view){
		this.model=model;
		this.view=view;
		view.addStartActionListener(new StartActionListener());
	}
	
	class StartActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			if(!view.checkData()){
				return;
			}
			view.getLogger().setText("");
			view.getResults().setText("");
			Thread t=new Thread(){
				
				@Override
				public void run(){
					model.setData(view.getOpeningTime(), 
							view.getRunningTime(), view.getMinArrTime(), view.getMaxArrTime(), 
							view.getMinServTime(), view.getMaxServTime(), view.getQueueNr());
					model.startQueues(new TextAreaPrintable(view.getLogger()),new TextAreaPrintable(view.getResults()));
				}
			};
			t.start();
		}

	}
}
