package controller;

import model.*;
import view.*;

public class QueueSim {
	
	/**
	 * The main method
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		MainPage view=new MainPage();
		Model model=new Model();
		Controller cont=new Controller(model, view);
		
		view.setVisible(true);
	}
}
