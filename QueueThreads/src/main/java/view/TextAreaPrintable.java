package view;

import javax.swing.JTextArea;

public class TextAreaPrintable implements Printable {
	private JTextArea textArea;
	
	/**
	 * Constructor for the TextAreaPrintabl object
	 * 
	 * @param jTA the object's JTextArea
	 */
	public TextAreaPrintable(JTextArea jTA){
		textArea=jTA;
	}

	public synchronized void print(String arg) {
		textArea.append(arg+"\n");
	}

}
