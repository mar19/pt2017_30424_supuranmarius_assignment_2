package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;

public class MainPage extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JButton btnStart;
	private JTextArea textArea;
	private JTextArea textArea_1;

	/**
	 * Create the frame.
	 */
	public MainPage() {
		setTitle("Shop");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 816, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogger = new JLabel("Logger");
		lblLogger.setBounds(12, 13, 56, 16);
		contentPane.add(lblLogger);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 32, 480, 219);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		
		JLabel lblResults = new JLabel("Results");
		lblResults.setBounds(12, 264, 56, 16);
		contentPane.add(lblResults);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 293, 478, 147);
		contentPane.add(scrollPane_1);
		
		textArea_1 = new JTextArea();
		scrollPane_1.setViewportView(textArea_1);
		textArea_1.setEditable(false);
		
		btnStart = new JButton("Start");
		btnStart.setBounds(605, 415, 97, 25);
		contentPane.add(btnStart);
		
		JPanel panel = new JPanel();
		panel.setBounds(523, 32, 263, 37);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("Opening time");
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setText("8");
		panel.add(textField);
		textField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(523, 82, 263, 37);
		contentPane.add(panel_1);
		
		JLabel lblRunningTimemins = new JLabel("Running time (hours)");
		panel_1.add(lblRunningTimemins);
		
		textField_1 = new JTextField();
		textField_1.setText("1");
		textField_1.setColumns(10);
		panel_1.add(textField_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(523, 132, 263, 37);
		contentPane.add(panel_2);
		
		JLabel lblMinArrivalTime = new JLabel("Min arrival time (mins)");
		panel_2.add(lblMinArrivalTime);
		
		textField_2 = new JTextField();
		textField_2.setText("1");
		textField_2.setColumns(10);
		panel_2.add(textField_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(523, 182, 263, 37);
		contentPane.add(panel_3);
		
		JLabel lblMaxArrivalTime = new JLabel("Max arrival time (mins)");
		panel_3.add(lblMaxArrivalTime);
		
		textField_3 = new JTextField();
		textField_3.setText("2");
		textField_3.setColumns(10);
		panel_3.add(textField_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(523, 232, 263, 37);
		contentPane.add(panel_4);
		
		JLabel lblMinServiceTime = new JLabel("Min service time (mins)");
		panel_4.add(lblMinServiceTime);
		
		textField_4 = new JTextField();
		textField_4.setText("1");
		textField_4.setColumns(10);
		panel_4.add(textField_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(523, 282, 263, 37);
		contentPane.add(panel_5);
		
		JLabel lblMaxServiceTime = new JLabel("Max service time (mins)");
		panel_5.add(lblMaxServiceTime);
		
		textField_5 = new JTextField();
		textField_5.setText("2");
		textField_5.setColumns(10);
		panel_5.add(textField_5);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(523, 332, 263, 37);
		contentPane.add(panel_6);
		
		JLabel lblNrOfQueues = new JLabel("Nr of queues");
		panel_6.add(lblNrOfQueues);
		
		textField_6 = new JTextField();
		textField_6.setText("2");
		textField_6.setColumns(10);
		panel_6.add(textField_6);
	}
	
	/**
	 * Add an ActionListener to the JButton
	 * 
	 * @param al the ActionListener
	 */
	public void addStartActionListener(ActionListener al){
		btnStart.addActionListener(al);
	}

	/**
	 * @return the textField
	 */
	public int getOpeningTime() {
		return Integer.parseInt(textField.getText());
	}

	/**
	 * @return the textField_1
	 */
	public int getRunningTime() {
		return Integer.parseInt(textField_1.getText());
	}

	/**
	 * @return the textField_2
	 */
	public int getMinArrTime() {
		return Integer.parseInt(textField_2.getText());
	}

	/**
	 * @return the textField_3
	 */
	public int getMaxArrTime() {
		return Integer.parseInt(textField_3.getText());
	}

	/**
	 * @return the textField_4
	 */
	public int getMinServTime() {
		return Integer.parseInt(textField_4.getText());
	}

	/**
	 * @return the textField_5
	 */
	public int getMaxServTime() {
		return Integer.parseInt(textField_5.getText());
	}

	/**
	 * @return the textField_6
	 */
	public int getQueueNr() {
		return Integer.parseInt(textField_6.getText());
	}
	
	/**
	 * @return the textArea
	 */
	public JTextArea getLogger() {
		return textArea;
	}

	/**
	 * @return the textArea_1
	 */
	public JTextArea getResults() {
		return textArea_1;
	}

	/**
	 * checks the data from the text fields for correctness.
	 * If it is incorrect, it prompts a message dialog
	 * 
	 * @return true if the data is correct, false otherwise
	 */
	public boolean checkData(){
		int nr=0;
		String msg="";
		try {
			nr=Integer.parseInt(textField.getText());
			if(nr<8 || nr>19){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Starting time must be between 8 and 19;\n";
		}
		try {
			nr=Integer.parseInt(textField_1.getText());
			if(nr<1 || nr>5){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Running time must be between 1 and 5;\n";
		}
		try {
			nr=Integer.parseInt(textField_2.getText());
			if(nr<1 || nr>10){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Min arrival time must be between 1 and 10;\n";
		}
		try {
			if(nr>Integer.parseInt(textField_3.getText())){
				throw new NumberFormatException();
			}
			nr=Integer.parseInt(textField_3.getText());
			if(nr<1 || nr>10){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Max arrival must be between 1 and 10, greater than the min arrival time;\n";
		}
		try {
			nr=Integer.parseInt(textField_4.getText());
			if(nr<1 || nr>10){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Min service time must be between 1 and 10;\n";
		}
		try {
			if(nr>Integer.parseInt(textField_5.getText())){
				throw new NumberFormatException();
			}
			nr=Integer.parseInt(textField_5.getText());
			if(nr<1 || nr>10){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Max service must be between 1 and 10, greater than the min service time;\n";
		}
		try {
			nr=Integer.parseInt(textField_6.getText());
			if(nr<1 || nr>5){
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			msg+="Number of queues must be between 1 and 5;\n";
		}
		if(!msg.equals("")){
			JOptionPane.showMessageDialog(contentPane, msg);
			return false;
		}
		return true;
	}
}
