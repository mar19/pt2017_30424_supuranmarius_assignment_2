package view;

public interface Printable {
	
	/**
	 * Print the String parameter
	 * 
	 * @param arg the String to be printed
	 */
	void print(String arg);
}
